# House Sales in King County

This is a Notebook for the Kaggle dataset, House Sales in King County: 

https://www.kaggle.com/datasets/harlfoxem/housesalesprediction

## Goal

The goal of this notebook is to build a custom regressor from scratch and try it on the dataset.
The following features will be implemented:
* **Methods**:
    * **fit**(X, y): fit linear model with my custom regressor.
    * **predict**(X): predict using my custom regressor.
    * **get_metrics**(): get a summary of the metrics.
* **Parameters**:
    * **fit_intercept**: default=True<br>
      Whether the intercept should be estimated or not. If False, the data is assumed to be already centered.
    * **alpha**: default=0<br>
      Constant that multiplies the L2 regularization term. The higher the value, the stronger the regularization.
    * **scaler**: default=False<br>
      If True, the regressors X will be scaled before regression by subtracting the mean and dividing by the l2-norm.
    * **solver**: {'ne', 'grad'}, default='ne'<br>
      Solver to use in the computational routines: when it is possible 'ne' uses the normal equation $\hat{\beta}=(X^TX+\lambda I)^{-1} X^Ty$, otherwise it is helped by SVD; 'grad' uses the Gradient descent. 
    * **number_of_iterations**: default=1000<br>
      The number of iterations when gradient solver is chosen. 
    * **learning_rate**: default=0.1<br>
      When gradient solver is chosen, it is the parameter that controls how much the model has to change in response to the estimated error each time the weights are updated.
    * **verbose**: default=True<br>
      Make gradient solver verbose
      
The regressor will have to return: 
- some metrics after the fit
- a summary of metrics
- the predicted target

In addition, the custom regressor will have to be able to use Scikit-learn tools, i.e. cross_val_score.

## Requirements

The requirements file contains all the packages needed to work through this notebook
