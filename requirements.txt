jupyterlab==3.5.3
matplotlib==3.6.3
pandas==1.5.3
pandas-datareader==0.10.0
scikit-learn==1.1.3
scipy==1.10.0
seaborn==0.12.2
statsmodels==0.13.5
